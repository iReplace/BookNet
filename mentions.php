<?php

    session_start();
    // On vérifie que l'utilisateur est connecté
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        $user = $_SESSION['user'];
    } else {
        // Sinon on définit utilisateur à null, c'est à dire rien
        $user = null;
    }

    // On définit la route actuelle pour l'affichage dans la navigation
    $route = '';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Mentions légales</title>

    <!-- Bootstrap -->
    <link href="css/flatly.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/fonts.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>
<body class="<?php if ($user === null): ?>bg-grey<?php endif?>">

    <?php
    // Si l'utilisateur n'est pas null c'est qu'il est connecté, on affiche donc la barre de navigation
        if ($user !== null) {
            include ('navigation.php');
        }
    ?>

    <div class="<?php if ($user !== null): ?>page-wrapper<?php else: ?>container<?php endif?>">
        <div class="rowbox">
            <h3 class="text-uppercase <?php if ($user === null): ?>text-center<?php endif?>">Mentions légales</h3>
        </div>

        <div class="rowbox">
            <div class="charte-container">

                <h3 class="text-uppercase border-bottom">Informations</h3>
                <p>
                    <b>Propriétaire :</b> Jean-Baptiste CAPLAN – Orléans, France<br>
                    <br>
                    <b>Créateurs :</b> Jean-Baptiste CAPLAN & Seyma TASDEMIR<br>
                    <br>
                    <b>Hébergeur :</b> OVH – 2 rue Kellermann – 59100 Roubaix – France.
                </p>

                <h3 class="text-uppercase border-bottom">Réglèment du site</h3>
                <p>
                    Toutes les règles à suivre pour utiliser notre site sont disponibles dans notre <a href="charte.php">charte d'utilisation</a>
                </p>
            </div>
        </div>

        <footer>
            <div class="text-center">
                <a href="../mentions.php">Mentions légales</a> - <a href="../charte.php">Charte d'utilisation</a> - <a href="../licences.php">Licences</a>
            </div>
        </footer>
    </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

<script>
    $('.equaliseh').each(function () {
        var height = 0;
        $($(this).data('target'), $(this)).each(function() {
            if ($(this).height() > height) {
                height = $(this).height()
            }
        });
        $($(this).data('target'), $(this)).height(height)
    })
</script>
</body>
</html>