<?php

    session_start();
    // On vérifie que l'utilisateur est connecté
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        $user = $_SESSION['user'];
    } else {
        // Sinon on l'envoie vers la page "vitrine" qui présente les fonctions et propose de s'inscrire
        header('Location: ../login.php');
        // TODO: Page vitrine et redirection
        exit('Redirection... <a href="">Cliquez ici</a>');
    }
    // On définit la route actuelle pour l'affichage dans la navigation
    $route = 'auteur';

    // On créer un tableau global pour stocker les potentielles erreurs
    $GLOBALS['erreurs'] = array();

    // On créer un dictionnaire global pour enregistrer les informations des différents champs du formulaire
    $GLOBALS['dictionnaire'] = array(
        'auteur_nom' => array('maxlength' => 30, 'label' => 'nom auteur', 'type' => 'string'),
        'auteur_prenom' => array('maxlength' => 30, 'label' => 'prénom auteur', 'type' => 'string'),
        'auteur_pseudo' => array('maxlength' => 30, 'label' => 'pseudonyme auteur', 'type' => 'string'),
        'auteur_naissance' => array('min' => -3000, 'max' => 2016, 'label' => 'année de naissance', 'type' => 'integer'),
        'auteur_mort' => array('min' => -3000, 'max' => 2016, 'label' => 'année de décès', 'type' => 'integer'),
        'auteur_biographie' => array('maxlength' => 3000, 'label' => 'biographie', 'type' => 'string')
    );

    // Tentative connexion à la base de données
    try {
        $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    } catch (Exception $e) {
        // En cas d'erreur on quitte proprement en affichant un message controllé
        die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
    }

    // On récupère l'id de l'auteur donné si il est fournit sinon on met en valeur par défaut -1 c'est à dire aucun auteur
    $auteur_id = (isset($_GET['auteur_id'])) ? $_GET['auteur_id'] : -1;

    $query = $db->prepare("SELECT
                          auteur.id AS auteur_id,
                          auteur.nom,
                          auteur.prenom,
                          IF(auteur.pseudo IS NULL, CONCAT(auteur.prenom, ' ', auteur.nom), auteur.pseudo) AS auteur_shortname,
                          -- Si le pseudo est NULL on recupère un '?'
                          IFNULL(auteur.pseudo, '?') AS pseudo,
                          -- Si la date de naissance est NULL on recupère un '?'
                          IFNULL(auteur.naissance, '?') AS naissance,
                          -- Si la date de décès est NULL on recupère un '?'
                          IFNULL(auteur.mort, '?') AS mort,
                          auteur.biographie
                          FROM auteur
                          WHERE auteur.id = ?");
    // On execute la requête en passant en argument l'id de l'auteur voulu
    $query->execute(array($auteur_id));

    // Aucun auteur ne correpond aux critères
    if ($query->rowCount() == 0) {
            header('Location: index.php');
            exit('Redirection... <a href="index.php">Cliquez ici</a>');
    }

    $auteur = $query->fetch();

    // On initialise avec des valeurs de l'auteur
    $auteur_nom = $auteur['nom'];
    $auteur_prenom = $auteur['prenom'];
    $auteur_pseudo = $auteur['pseudo'];
    $auteur_naissance = $auteur['naissance'];
    $auteur_mort = $auteur['mort'];
    $auteur_biographie = $auteur['biographie'];

    // Si on détecte une valeur dans $_POST, c'est ce que le formulaire à été soumis
    if (isset($_POST['auteur_nom'])) {

        // On récupère chaque valeur à l'aide de notre fonction retrieve_input() qui efectue tous les tests pour
        // singalé une potentielle erreur par rapport aux informations indiquées dans le dictionnaire global
        $auteur_nom = retrieve_input('auteur_nom');
        $auteur_prenom = retrieve_input('auteur_prenom');
        $auteur_pseudo = retrieve_input('auteur_pseudo');
        $auteur_naissance = retrieve_input('auteur_naissance');
        $auteur_mort = retrieve_input('auteur_mort');
        $auteur_biographie = retrieve_input('auteur_biographie');

        // Si il n'y a pas d'erreur
        if (count($GLOBALS['erreurs']) == 0) {

            // On vérifie si l'auteur existe
            // préparation de la requête
            $query = $db->prepare('SELECT id
                                  FROM auteur
                                  WHERE
                                  id = ?');
            $query->execute(array(
                $auteur_id
            ));
            if ($query->rowCount() != 1) {
                // On ajoute une erreur
                $GLOBALS['erreurs'][] = "L'auteur \"$auteur_prenom $auteur_nom\" n'existe pas";
            }

            // Si il n'y a pas d'erreur on procède à la suite
            if (count($GLOBALS['erreurs']) == 0) {

                // On prépare la requête de mise à jour
                $query = $db->prepare('UPDATE auteur SET
                                        nom = ?,
                                        prenom = ?,
                                        pseudo = ?,
                                        naissance = ?,
                                        mort = ?,
                                        biographie = ?,
                                        utilisateur_id = ?
                                        WHERE
                                        id = ?');
                // On l'execute en passant les valeurs
                $query->execute(array(
                    $auteur_nom,
                    $auteur_prenom,
                    $auteur_pseudo,
                    $auteur_naissance,
                    $auteur_mort,
                    $auteur_biographie,
                    $user['id'],
                    $auteur_id
                ));

                // On redirige vers la fiche de l'auteur
                header('Location: fiche.php?auteur_id='.$auteur_id);
                exit('Redirection... <a href="fiche.php?auteur_id='.$auteur_id.'">Cliquez ici</a>');
            }
        }
    }

function retrieve_input($input_name) {
    // On récupère la valeur depuis $_POST
    $value = $_POST[$input_name];
    $informations = $GLOBALS['dictionnaire'][$input_name];
    if ($informations['type'] == 'string') {
        // On convertit value en string
        $value = strval($value);
        // Si la valeur est un "?" on renvoit null, c'est à dire rien
        if ($value == '?') {
            return null;
        }
        // On récupère la longeur minimum ou 1 si elle n'est pas définit
        $minlength = (isset($informations['minlength'])) ? $informations['minlength'] : 1;
        // On récupère la longeur maximum ou 2 si elle n'est pas définit
        $maxlength = (isset($informations['maxlength'])) ? $informations['maxlength'] : 4;
        // Si la longueur de la chaine dépasse la longueur maximum ou est inférieur à la longueur minimum on ajoute un erreur
        if (strlen($value) < $minlength || strlen($value) > $maxlength) {
            $label = $informations['label'];
            $GLOBALS['erreurs'][] = "La longueur de $label n'est pas respectée (entre $minlength et $maxlength caractères)";
        }
        // Si aucune valeur n'est spécifiée on ajoute une erreur
        if ($value == '') {
            $label = $informations['label'];
            $GLOBALS['erreurs'][] = "Vous devez remplir \"$label\"";
        }

        // On sécurise la chaine de caractère en échappant tous les caractères spécifique à l'HTML
        $value = htmlspecialchars($value);
    } elseif ($informations['type'] == 'integer') {
        // Si aucune valeur n'est précisé on ajoute une erreur
        if ($value == '') {
            $label = $informations['label'];
            $GLOBALS['erreurs'][] = "Vous devez remplir \"$label\"";
        }
        // Si la valeur est un "?" on renvoit null, c'est à dire rien
        if ($value == '?') {
            return null;
        }
        // On convertit value en un entier
        $value = intval($value);
        // Si la valeur spécifiée dépasse les borne défini on ajoute une erreur
        if ($value < $informations['min'] || $value > $informations['max']) {
            $label = $informations['label'];
            $GLOBALS['erreurs'][] = "La valeur de $label n'est pas respectée";
        }
    } elseif ($informations['type'] == 'entity') {
        $value = intval($value);
        // Tentative connexion à la base de données
        try {
            $db = new PDO('mysql:host=jeanbafrix-1.mysql.db;dbname=jeanbafrix-1', 'jeanbafrix-1', 'MonPanda45', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        } catch (Exception $e) {
            // En cas d'erreur on quitte proprement en affichant un message controllé
            die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
        }
        // On prépare un requête pour vérifier si l'entité existe bel et bien
        $query = $db->prepare('SELECT * FROM '.$informations['entity'].' WHERE id = ?');
        $query->execute(array(
            $value
        ));
        if ($query->rowCount() != 1) {
            // Si elle n'existe pas on ajoute une erreur
            $label = $informations['label'];
            $GLOBALS['erreurs'][] = "La valeur de $label n'est pas respectée";
        }
    }

    // On retourne la valeur
    return $value;
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Edition de <?php echo $auteur['auteur_shortname'] ?></title>

        <!-- Bootstrap -->
        <link href="../css/flatly.min.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet">
        <link href="../css/fonts.css" rel="stylesheet">
        <link href="../css/chosen.min.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">

    </head>
    <body>
        <?php include('../navigation.php') ?>

        <div class="page-wrapper">

            <div class="rowbox">
                <h2>Edition de <i><?php echo $auteur['auteur_shortname'] ?></i></h2>

                <hr>

                <form method="post" class="container">

                    <?php if (count($GLOBALS['erreurs']) > 0): ?>
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <? foreach($GLOBALS['erreurs'] as $erreur): ?>
                                    <li><?php echo $erreur ?></li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    <?php endif ?>

                    <div class="form-group">
                        <label>Prénom</label>
                        <input type="text" class="form-control" name="auteur_prenom" maxlength="30" placeholder="Prénom" value="<?php echo $auteur_prenom ?>">
                    </div>
                    <div class="form-group">
                        <label>Nom</label>
                        <input type="text" class="form-control" name="auteur_nom" maxlength="30" placeholder="Nom" value="<?php echo $auteur_nom ?>">
                    </div>
                    <div class="form-group">
                        <label>Pseudonyme</label>
                        <input type="text" class="form-control" name="auteur_pseudo" maxlength="30" placeholder="Pseudonyme (Ex: Voltaire)" value="<?php echo ($auteur_pseudo == null) ? $auteur_pseudo : '?' ?>">
                        <div class="help-block">Si votre auteur n'a pas de pseudonyme remplissez par un "?"</div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Année de naissance</label>
                            <input type="text" class="form-control" name="auteur_naissance" maxlength="4" placeholder="Année de naissance" value="<?php echo ($auteur_naissance) ? $auteur_naissance : '?' ?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Année de decès</label>
                            <input type="text" class="form-control" name="auteur_mort" maxlength="4" placeholder="Année de decès" value="<?php echo ($auteur_mort) ? $auteur_mort : '?' ?>">
                            <div class="help-block">Si votre auteur est encore en vie remplissez par un "?"</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Biographie</label>
                        <textarea class="form-control" name="auteur_biographie" rows="7" placeholder="Petite biographie"><?php echo $auteur_biographie ?></textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="fiche.php?auteur_id=<?php echo $auteur['auteur_id'] ?>" class="btn btn-primary btn-outline btn-block"><i class="fa fa-user fa-fw"></i> <?php echo $auteur['auteur_shortname'] ?></a>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-success btn-block">Sauvegarder</button>
                        </div>
                    </div>
                </form>
            </div>
            <footer>
                <div class="text-center">
                    <a target="_blank" href="../mentions.php">Mentions légales</a> - <a target="_blank" href="../charte.php">Charte d'utilisation</a> - <a target="_blank" href="../licences.php">Licences</a>
                </div>
            </footer>
        </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../js/bootstrap.min.js"></script>
        <!-- Chosen -->
        <script src="../js/chosen.jquery.min.js"></script>

        <script>
            $('.chosen-select').chosen();
        </script>
    </body>
</html>