<?php

    session_start();
    // On vérifie que l'utilisateur est connecté
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        $user = $_SESSION['user'];
    } else {
        // Sinon on l'envoie vers la page "vitrine" qui présente les fonctions et propose de s'inscrire
        header('Location: ../login.php');
        // TODO: Page vitrine et redirection
        exit('Redirection... <a href="">Cliquez ici</a>');
    }

    // Si aucun auteur_id n'est spécifier on renvoie vers la liste des critique
    if (!isset($_GET['auteur_id'])) {
        header('Location: index.php');
        exit('Redirection... <a href="../auteur/index.php">Cliquez ici</a>');
    }

    $auteur_id = $_GET['auteur_id'];

    // Tentative connexion à la base de données
    try {
        $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    } catch (Exception $e) {
        // En cas d'erreur on quitte proprement en affichant un message controllé
        die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
    }

    // Cette requête récupère le like de la critique par l'utilisateur
    $query = $db->prepare('SELECT
                            *
                            FROM utilisateur_auteurspreferes
                            WHERE
                            utilisateur_id = ?
                            AND auteur_id = ?');
    $query->execute(array(
        $user['id'],
        $auteur_id
    ));

    // Si il n'y a pas de rang correspondant au auteur et à l'utilisateur c'est qu'il ne l'a pas dans ses favoris
    if ($query->rowCount() == 0) {
        $query = $db->prepare('INSERT INTO utilisateur_auteurspreferes
                                   -- VALUES(utilisateur_id, auteur_id)
                                   VALUES(?, ?)');
        $query->execute(array($user['id'], $auteur_id));

    } else {
        // Sinon il y'en a déjà une et donc on la supprime
        $query = $db->prepare('DELETE FROM utilisateur_auteurspreferes
                                    WHERE
                                    utilisateur_id = ?
                                    AND auteur_id = ?');
        $query->execute(array(
            $user['id'],
            $auteur_id
        ));
    }

    header("Location: fiche.php?auteur_id=$auteur_id");
    exit('Redirection... <a href="fiche.php?auteur_id='.$auteur_id.'">Cliquez ici</a>');

