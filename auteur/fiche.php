<?php
    session_start();
    // On vérifie que l'utilisateur est connecté
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        $user = $_SESSION['user'];
    } else {
        // Sinon on l'envoie vers la page "vitrine" qui présente les fonctions et propose de s'inscrire
        header('Location: ../login.php');
        // TODO: Page vitrine et redirection
        exit('Redirection... <a href="">Cliquez ici</a>');
    }
    // On définit la route actuelle pour l'affichage dans la navigation
    $route = 'auteur';

    // On créer un tableau global pour stocker les potentielles erreurs
    $GLOBALS['erreurs'] = array();

    if (isset($_GET['auteur_id'])) {
        // Tentative connexion à la base de données
        try {
            $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        } catch (Exception $e) {
            // En cas d'erreur on quitte proprement en affichant un message controllé
            die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
        }

        // préparation de la requête pour récupérer l'auteur
        $smallTag = ['<small>', '</small>'];
        $utilisateur_id = $user['id'];
        $query = $db->prepare("SELECT
                              auteur.id AS auteur_id,
                              auteur.pseudo,
                              -- Si auteur.pseudo est NULL on récupère une string concaténant prenom et nom
                              -- Sinon on recupère une string concatéant le pseudo avec le prenom et le nom en petit
                              IF(auteur.pseudo IS NULL, CONCAT(auteur.prenom, ' ', auteur.nom), CONCAT(auteur.pseudo, ' $smallTag[0]', auteur.prenom, ' ', auteur.nom, '$smallTag[1]')) AS auteur_fullname,
                              IF(auteur.pseudo IS NULL, CONCAT(auteur.prenom, ' ', auteur.nom), auteur.pseudo) AS auteur_shortname,
                              -- On utilise des sous-requete pour récupérer le nombre de favoris sur cet auteur
                              (SELECT COUNT(*) FROM utilisateur_auteurspreferes WHERE utilisateur_auteurspreferes.auteur_id = auteur.id) AS count_auteurprefere,
                              -- Si la date de naissance est NULL on recupère un '?'
                              -- On utilise des sous-requete pour récupérer le fait que l'utilisateur à ajouter l'auteur dans ses favoris
                              EXISTS (SELECT * FROM utilisateur_auteurspreferes WHERE utilisateur_auteurspreferes.auteur_id = auteur.id AND utilisateur_auteurspreferes.utilisateur_id = $utilisateur_id) AS utilisateur_auteurprefere,
                              IFNULL(auteur.naissance, '?') AS naissance,
                              -- Si la date de décès est NULL on recupère un
                              IFNULL(auteur.mort, '?') AS mort,
                              auteur.biographie,
                              utilisateur.id AS utilisateur_id,
                              CONCAT(utilisateur.prenom, ' ', utilisateur.nom) AS utilisateur_fullname
                              FROM auteur
                              LEFT JOIN utilisateur ON utilisateur.id = auteur.utilisateur_id
                              WHERE auteur.id = ?");
        // On execute la requête en passant en argument l'id de l'auteur voulu
        $query->execute(array($_GET['auteur_id']));
        if ($query->rowCount() == 1) {
            // Si il y'a bien un auteur on stocke ses infos dans une variable
            $auteur = $query->fetch();
            // Si l'auteur existe, on va récupérer les livre qui lui sont associés
            $query = $db->prepare('SELECT *
                                  FROM livre
                                  WHERE
                                  auteur_id = ?
                                  -- On récupère par ordre chronologique
                                  ORDER BY date');
            // On execute la requête en passant en argument l'id du livre demandé
            $query->execute(array($auteur['auteur_id']));
            // On stockes les critiques sous l'index "critiques" de la variable dictionnaire "livre"
            $auteur['livres'] = $query->fetchAll();
        } else {
            // Sinon on place la valeur de la variable sur null, c'est à dire rien
            $auteur = null;
            // On ajoute une erreur pour l'afficher
            $GLOBALS['erreurs'][] = "L'auteur demandé n'existe pas...";
        }

        # On vérfie si on a transmis un livre via "from"
        if (isset($_GET['from'])) {
            $livre_id = $_GET['from'];

            // préparation de la requête pour récupérer le livre
            $query = $db->prepare('SELECT
                                   *
                                   FROM livre
                                   WHERE
                                   id = ?');
            $query->execute(array($livre_id));
            if ($query->rowCount() == 1) {
                // Si il y'a bien un livre on stocke ses infos dans une variable
                $livre_from = $query->fetch();
            } else {
                // Sinon on place la valeur de la variable sur null, c'est à dire rien
                $livre_from = null;
            }
        }

    } else {
        // Si aucun auteur n'est fournit on redirige vers la page index.php du dossier auteur
        header('Location: index.php');
        // Si pour un raison X ou Y la redirection n'a pas lieu on quitte la page dans tout les cas avec un lien vers
        // la page voulue
        exit('Redirection... <a href="index.php">Cliquer ici</a>');
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Fiche: <?php echo $auteur['auteur_shortname'] ?></title>

        <!-- Bootstrap -->
        <link href="../css/flatly.min.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet">
        <link href="../css/fonts.css" rel="stylesheet">
        <link href="../css/chosen.min.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">

    </head>
    <body>
        <?php include('../navigation.php') ?>

        <div class="page-wrapper">


            <?php if (count($GLOBALS['erreurs']) > 0): ?>
                <div class="rowbox">
                    <div class="alert alert-danger" role="alert">
                        <ul>
                            <? foreach($GLOBALS['erreurs'] as $erreur): ?>
                                <li><?php echo $erreur ?></li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>
            <?php endif ?>

            <?php if (isset($auteur) && $auteur): ?>
                <div class="rowbox">
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-1">
                            <p style="padding-top: 25px" class="text-center"><i class="fa fa-user fa-5x"></i></p>
                        </div>
                        <div class="col-sm-9">
                            <h2><em><strong><?php echo $auteur['auteur_fullname'] ?></strong></em> <small class="text-warning"><i class="fa fa-star fa-fw"></i> <?php echo $auteur['count_auteurprefere'] ?></small></h2>
                            <h4><span class="label label-success"><?php echo $auteur['naissance'].' - '.$auteur['mort'] ?></span></h4>
                        </div>

                        <div class="col-md-10 col-md-offset-1">
                            <p class="text-right small no-margin">Dernières modifications par : <a href="../profil/index.php?utilisateur_id=<?php echo $auteur['utilisateur_id'] ?>"><?php echo $auteur['utilisateur_fullname'] ?></a></p>

                            <div class="well well-lg">
                                <p><?php echo nl2br($auteur['biographie']) ?></p>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <a href="favorite.php?auteur_id=<?php echo $auteur['auteur_id'] ?>" class="btn btn-success btn-outline btn-block btn-sm">
                                        <?php if ($auteur['utilisateur_auteurprefere'] > 0): ?>
                                            <i class="fa fa-star fa-fw"></i> Retirer de mes favoris
                                        <?php else: ?>
                                            <i class="fa fa-star-o fa-fw"></i> Ajouter à mes favoris
                                        <?php endif ?>
                                    </a>
                                </div>
                                <div class="col-sm-6">
                                    <a href="editer.php?auteur_id=<?php echo $auteur['auteur_id'] ?>" class="btn btn-success btn-outline btn-block btn-sm"><i class="fa fa-pencil fa-fw"></i> Editer la fiche</a>
                                </div>
                            </div>
                            <div style="margin-top: 15px" class="row">
                                <div class="col-sm-12">
                                    <?php if (isset($livre_from) && $livre_from): ?>
                                        <a target="_self" class="btn btn-primary btn-outline btn-block btn-sm" href="../livre/fiche.php?livre_id=<?php echo $livre_from['id'] ?>"><i class="fa fa-book fa-fw"></i> <i><?php echo $livre_from['titre'] ?></i></a>
                                    <?php else: ?>
                                        <a target="_self" class="btn btn-primary btn-outline btn-block btn-sm" href="index.php"><i class="fa fa-list fa-fw"></i> Liste des auteurs</a>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="rowbox">
                    <div class="container-fluid">

                        <h3 style="margin-bottom: 25px"><span class="text-uppercase">Livres</span> <a href="../livre/ajouter.php?auteur_id=<?php echo $auteur['auteur_id'] ?>" class="btn btn-success btn-outline btn-xs text">Ajouter <i class="fa fa-plus fa-fw"></i></a></h3>


                        <div>
                            <ul class="list-inline list-item list-livre">
                                <?php if(count($auteur['livres']) == 0): ?>
                                    <p class="text-center">Aucun livre n'a encore été enregistré pour <i><?php echo $auteur['auteur_fullname'] ?></i>, soyer le premier à en <a href="../livre/ajouter.php?auteur_id=<?php echo $auteur['auteur_id'] ?>">ajouter un</a> !</p>
                                <?php endif ?>
                                <?php foreach($auteur['livres'] as $livre): ?>
                                    <li class="text-center">
                                        <a href="../livre/fiche.php?livre_id=<?php echo $livre['id'] ?>&from=<?php echo $auteur['auteur_id'] ?>">
                                            <div style="top: 25px" class="titre">
                                                <p><i><?php echo $livre['titre'] ?></i></p>
                                            </div>

                                            <div style="bottom: 15px" class="infos">
                                                <p>(<?php echo $livre['date'] ?>)</p>
                                            </div>
                                        </a>
                                    </li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php endif ?>
            <footer>
                <div class="text-center">
                    <a href="../mentions.php">Mentions légales</a> - <a href="../charte.php">Charte d'utilisation</a> - <a href="../licences.php">Licences</a>
                </div>
            </footer>
        </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../js/bootstrap.min.js"></script>
        <!-- Chosen -->
        <script src="../js/chosen.jquery.min.js"></script>

        <script>
            $('.chosen-select').chosen();
        </script>
    </body>
</html>