<?php

    session_start();

    // Si l'utilisateur est déjà connecté
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        header('Location: index.php');
        exit('Redirection... <a href="index.php">Cliquez ici</a>');
    }


    // On crée un tableau global pour stocker les potentielles erreurs
    $GLOBALS['erreurs'] = array();

    // On crée un dictionnaire global pour enregistrer les informations des différents champs du formulaire
    $GLOBALS['dictionnaire'] = array(
        'lastname' => array('maxlength' => 30, 'minlength' => 2, 'label' => 'nom', 'type' => 'string'),
        'firstname' => array('maxlength' => 30, 'minlength' => 2, 'label' => 'prénom', 'type' => 'string'),
        'pseudo' => array('maxlength' => 25, 'minlength' => 3, 'label' => 'pseudo', 'type' => 'string'),
        'email' => array('maxlength' => 255, 'label' => 'email', 'type' => 'string'),
        'password' => array('maxlength' => 35, 'minlength' => 8, 'label' => 'mot de passe', 'type' => 'string'),
        'birthday_day' => array('min' => 1, 'max' => 31, 'label' => 'jour de naissance', 'type' => 'integer'),
        'birthday_month' => array('min' => 1, 'max' => 12, 'label' => 'mois de naissance', 'type' => 'integer'),
        'birthday_year' => array('min' => 1910, 'max' => 2016, 'label' => 'année de naissance', 'type' => 'integer')
    );

    // On initialise les valeurs par défaut
    $lastname = '';
    $firstname = '';
    $pseudo = '';
    $email = '';
    $password = '';
    $birthday_day = '';
    $birthday_month = '';
    $birthday_year = '';

    if (isset($_POST['lastname'])) {
        // On récupère les valeurs
        $lastname = retrieve_input('lastname');
        $firstname = retrieve_input('firstname');
        $pseudo = retrieve_input('pseudo');
        $email = retrieve_input('email');
        $password = retrieve_input('password');
        $birthday_day = retrieve_input('birthday_day');
        $birthday_month = retrieve_input('birthday_month');
        $birthday_year = retrieve_input('birthday_year');

        // Si il n'y a pas d'erreur on procède à la suite
        if (count($GLOBALS['erreurs']) == 0) {
            // Tentative connexion à la base de données
            try {
                $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
            } catch (Exception $e) {
                // En cas d'erreur on quitte proprement en affichant un message controllé
 die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
            }

            // On vérifie si le pseudo est déjà utilisé
            // préparation de la requête
            $query = $db->prepare('SELECT pseudo FROM utilisateur WHERE pseudo = ?');
            $query->execute(array($pseudo));
            if ($query->rowCount() > 0) {
                $GLOBALS['erreurs'][] = "Le pseudo \"$pseudo\" est déjà utilisé";
            }

            // On vérifie si l'email est déjà utilisé
            // préparation de la requête
            $query = $db->prepare('SELECT email FROM utilisateur WHERE email = ?');
            $query->execute(array($email));
            if ($query->rowCount() > 0) {
                $GLOBALS['erreurs'][] = "L'email \"$email\" est déjà utilisé";
            }

            // Si il n'y a pas d'erreur on procède à la suite
            if (count($GLOBALS['erreurs']) == 0) {

                // On définit des valeurs par défaut
                $avatar = 'default_profile.png';
                // On hash le mot de passe avec password_hash() qui amène une protection très forte face aux tentative
                // par brute force en hachant plusieurs fois le mot de passe
                $password = password_hash($password, PASSWORD_DEFAULT);
                exit($password);
                // On prépare la requête d'ajout
                $query = $db->prepare('INSERT INTO utilisateur
                                      -- VALUES(id, pseudo, prenom, nom, password, email, avatar)
                                      VALUES(NULL, ?, ?, ?, ?, ?, ?)');
                // On l'execute en passant les valeurs
                $query->execute(array(
                    $pseudo,
                    $firstname,
                    $lastname,
                    $password,
                    $email,
                    $avatar
                ));

                header('Location: login.php?from=inscription&email='.urlencode($email));
                exit('Redirection... <a href="login.php?from=inscription&email='.urlencode($email).'">Cliquez ici</a>');
            }
        }
    }

function retrieve_input($input_name) {
    // On récupère la valeur depuis $_POST
    $value = $_POST[$input_name];
    $informations = $GLOBALS['dictionnaire'][$input_name];
    if ($informations['type'] == 'string') {
        // On convertit value en string
        $value = strval($value);
        // On récupère la longeur minimum ou 1 si elle n'est pas définit
        $minlength = (isset($informations['minlength'])) ? $informations['minlength'] : 1;
        // On récupère la longeur maximum ou 2 si elle n'est pas définit
        $maxlength = (isset($informations['maxlength'])) ? $informations['maxlength'] : 4;
        // Si la longueur de la chaine dépasse la longueur maximum ou est inférieur à la longueur minimum on ajoute un erreur
        if (strlen($value) < $minlength || strlen($value) > $maxlength) {
            $label = $informations['label'];
            $GLOBALS['erreurs'][] = "La longueur de $label n'est pas respectée (entre $minlength et $maxlength caractères)";
        }
        // Si aucune valeur n'est spécifiée on ajoute une erreur
        if ($value == '') {
            $label = $informations['label'];
            $GLOBALS['erreurs'][] = "Vous devez remplir \"$label\"";
        }

        // On sécurise la chaine de caractère en échappant tous les caractères spécifique à l'HTML
        $value = htmlspecialchars($value);
    } elseif ($informations['type'] == 'integer') {
        // Si aucune valeur n'est précisé on ajoute une erreur
        if ($value == '') {
            $label = $informations['label'];
            $GLOBALS['erreurs'][] = "Vous devez remplir \"$label\"";
        }
        // On convertit value en un entier
        $value = intval($value);
        // Si la valeur spécifiée dépasse les borne défini on ajoute une erreur
        if ($value < $informations['min'] || $value > $informations['max']) {
            $label = $informations['label'];
            $GLOBALS['erreurs'][] = "La valeur de $label n'est pas respectée";
        }
    } elseif ($informations['type'] == 'entity') {
        $value = intval($value);
        // Tentative connexion à la base de données
        try {
            $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        } catch (Exception $e) {
            // En cas d'erreur on quitte proprement en affichant un message controllé
            die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
        }
        // On prépare un requête pour vérifier si l'entité existe bel et bien
        $query = $db->prepare('SELECT * FROM '.$informations['entity'].' WHERE id = ?');
        $query->execute(array(
            $value
        ));
        if ($query->rowCount() != 1) {
            // Si elle n'existe pas on ajoute une erreur
            $label = $informations['label'];
            $GLOBALS['erreurs'][] = "La valeur de $label n'est pas respectée";
        }
    }

    // On retourne la valeur
    return $value;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Inscription</title>

    <!-- Bootstrap -->
    <link href="css/flatly.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>
<body class="bg-white">
    <div class="row">
        <div id="inscription" class="col-md-4 col-md-offset-4">
            <h1 class="text-center">Inscription</h1>
            <?php if (count($GLOBALS['erreurs']) > 0): ?>
                <div class="alert alert-danger" role="alert">
                    <ul>
                        <? foreach($GLOBALS['erreurs'] as $erreur): ?>
                            <li><?php echo $erreur ?></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            <?php endif ?>
            <form method="post">
                <div class="form-group">
                    <label for="lastname">Nom</label>
                    <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Nom" maxlength="30" value="<?php echo $lastname ?>">
                </div>
                <div class="form-group">
                    <label for="prenom">Prenom</label>
                    <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Prenom" maxlength="30" value="<?php echo $firstname ?>">
                </div>
                <div class="form-group">
                    <label for="pseudo">Pseudo</label>
                    <input type="text" class="form-control" id="pseudo" name="pseudo" placeholder="Pseudo" maxlength="25" value="<?php echo $pseudo?>">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" maxlength="255" value="<?php echo $email ?>">
                </div>
                <div class="form-group">
                    <label for="Password">Mot de passe</label>
                    <input type="password" class="form-control" id="Password" name="password" placeholder="Mot de passe" maxlength="255">
                </div>
                Date de naissance
                <div class="row">
                    <div class="col-md-4">
                        <select class="form-control" name="birthday_day">
                            <option value="1" <?php if ($birthday_day == 1):?>selected<?php endif ?>>1</option>
                            <option value="2" <?php if ($birthday_day == 2):?>selected<?php endif ?>>2</option>
                            <option value="3" <?php if ($birthday_day == 3):?>selected<?php endif ?>>3</option>
                            <option value="4" <?php if ($birthday_day == 4):?>selected<?php endif ?>>4</option>
                            <option value="5" <?php if ($birthday_day == 5):?>selected<?php endif ?>>5</option>
                            <option value="6" <?php if ($birthday_day == 6):?>selected<?php endif ?>>6</option>
                            <option value="7" <?php if ($birthday_day == 7):?>selected<?php endif ?>>7</option>
                            <option value="8" <?php if ($birthday_day == 8):?>selected<?php endif ?>>8</option>
                            <option value="9" <?php if ($birthday_day == 9):?>selected<?php endif ?>>9</option>
                            <option value="10" <?php if ($birthday_day == 10):?>selected<?php endif ?>>10</option>
                            <option value="11" <?php if ($birthday_day == 11):?>selected<?php endif ?>>11</option>
                            <option value="12" <?php if ($birthday_day == 12):?>selected<?php endif ?>>12</option>
                            <option value="13" <?php if ($birthday_day == 13):?>selected<?php endif ?>>13</option>
                            <option value="14" <?php if ($birthday_day == 14):?>selected<?php endif ?>>14</option>
                            <option value="15" <?php if ($birthday_day == 15):?>selected<?php endif ?>>15</option>
                            <option value="16" <?php if ($birthday_day == 16):?>selected<?php endif ?>>16</option>
                            <option value="17" <?php if ($birthday_day == 17):?>selected<?php endif ?>>17</option>
                            <option value="18" <?php if ($birthday_day == 18):?>selected<?php endif ?>>18</option>
                            <option value="19" <?php if ($birthday_day == 19):?>selected<?php endif ?>>19</option>
                            <option value="20" <?php if ($birthday_day == 20):?>selected<?php endif ?>>20</option>
                            <option value="21" <?php if ($birthday_day == 21):?>selected<?php endif ?>>21</option>
                            <option value="22" <?php if ($birthday_day == 22):?>selected<?php endif ?>>22</option>
                            <option value="23" <?php if ($birthday_day == 23):?>selected<?php endif ?>>23</option>
                            <option value="24" <?php if ($birthday_day == 24):?>selected<?php endif ?>>24</option>
                            <option value="25" <?php if ($birthday_day == 25):?>selected<?php endif ?>>25</option>
                            <option value="26" <?php if ($birthday_day == 26):?>selected<?php endif ?>>26</option>
                            <option value="27" <?php if ($birthday_day == 27):?>selected<?php endif ?>>27</option>
                            <option value="28" <?php if ($birthday_day == 28):?>selected<?php endif ?>>28</option>
                            <option value="29" <?php if ($birthday_day == 29):?>selected<?php endif ?>>29</option>
                            <option value="30" <?php if ($birthday_day == 30):?>selected<?php endif ?>>30</option>
                            <option value="31" <?php if ($birthday_day == 31):?>selected<?php endif ?>>31</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <select class="form-control" name="birthday_month">
                            <option value="1" <?php if ($birthday_month == 1):?>selected<?php endif ?>>Janvier</option>
                            <option value="2" <?php if ($birthday_month == 2):?>selected<?php endif ?>>Février</option>
                            <option value="3" <?php if ($birthday_month == 3):?>selected<?php endif ?>>Mars</option>
                            <option value="4" <?php if ($birthday_month == 4):?>selected<?php endif ?>>Avril</option>
                            <option value="5" <?php if ($birthday_month == 5):?>selected<?php endif ?>>Mai</option>
                            <option value="6" <?php if ($birthday_month == 6):?>selected<?php endif ?>>Juin</option>
                            <option value="7" <?php if ($birthday_month == 7):?>selected<?php endif ?>>Juillet</option>
                            <option value="8" <?php if ($birthday_month == 8):?>selected<?php endif ?>>Aout</option>
                            <option value="9" <?php if ($birthday_month == 9):?>selected<?php endif ?>>Septembre</option>
                            <option value="10" <?php if ($birthday_month == 10):?>selected<?php endif ?>>Octobre</option>
                            <option value="11" <?php if ($birthday_month == 11):?>selected<?php endif ?>>Novembre</option>
                            <option value="12" <?php if ($birthday_month == 12):?>selected<?php endif ?>>Décembre</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <input type="number" class="form-control" id="birthday_year" name="birthday_year" placeholder="Année" maxlength="4" value="<?php echo $birthday_year ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="help-block">
                                En cliquant sur ce bouton, je reconnais avoir lu et accepté les conditions générales du site détaillées dans sa charte accessible <a target="_blank" href="charte.php">ici</a>
                            </div>
                            <button type="submit" class="btn btn-success btn-block">Inscription</button>
                        </div>
                        <div class="col-sm-6 col-sm-offset-3">
                            <hr>
                        </div>
                        <div class="col-md-12">
                            <div class="help-block text-center">
                                Déjà un compte ? <a href="login.php">Se connecter</a>
                            </div>
                        </div>
                    </div>

            </form>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>