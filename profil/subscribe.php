<?php

    session_start();
    // On vérifie que l'utilisateur est connecté
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        $user = $_SESSION['user'];
    } else {
        // Sinon on l'envoie vers la page "vitrine" qui présente les fonctions et propose de s'inscrire
        header('Location: ../login.php');
        // TODO: Page vitrine et redirection
        exit('Redirection... <a href="">Cliquez ici</a>');
    }

    // Si aucun utilisateur_id n'est spécifier on renvoie vers la liste des critique
    if (!isset($_GET['utilisateur_id'])) {
        header('Location: index.php');
        exit('Redirection... <a href="../auteur/index.php">Cliquez ici</a>');
    }

    $utilisateur_id = $_GET['utilisateur_id'];

    // Tentative connexion à la base de données
    try {
        $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    } catch (Exception $e) {
        // En cas d'erreur on quitte proprement en affichant un message controllé
        die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
    }

    // Si l'utilisateur tente de s'abonner à lui même
    if ($utilisateur_id == $user['id']) {
        // On le redirige sans procéder à l'abonnement
        header("Location: index.php?utilisateur_id=$utilisateur_id");
        exit('Redirection... <a href="index.php?utilisateur_id='.$utilisateur_id.'">Cliquez ici</a>');
    }

    // Cette requête récupère le like de la critique par l'utilisateur
    $query = $db->prepare('SELECT
                            *
                            FROM abonnements
                            WHERE
                            utilisateur_abonne_id = ?
                            AND utilisateur_suivi_id = ?');
    $query->execute(array(
        $user['id'],
        $utilisateur_id
    ));

    // Si il n'y a pas de rang correspondant au auteur et à l'utilisateur c'est qu'il ne l'a pas dans ses favoris
    if ($query->rowCount() == 0) {
        $query = $db->prepare('INSERT INTO abonnements
                               -- VALUES(NULL, utilisateur_abonne_id, utilisateur_suivi_id, date_action)
                               VALUES(NULL, ?, ?, NOW())');
        $query->execute(array($user['id'], $utilisateur_id));

    } else {
        // Sinon il y'en a déjà une et donc on la supprime
        $query = $db->prepare('DELETE FROM abonnements
                                    WHERE
                                    utilisateur_abonne_id= ?
                                    AND utilisateur_suivi_id = ?');
        $query->execute(array(
            $user['id'],
            $utilisateur_id
        ));
    }

    header("Location: index.php?utilisateur_id=$utilisateur_id");
    exit('Redirection... <a href="index.php?utilisateur_id='.$utilisateur_id.'">Cliquez ici</a>');

