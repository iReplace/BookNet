<?php

    session_start();
    // On vérifie que l'utilisateur est connecté
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        $user = $_SESSION['user'];
    } else {
        // Sinon on définit utilisateur à null, c'est à dire rien
        $user = null;
    }

    // On définit la route actuelle pour l'affichage dans la navigation
    $route = '';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Licenses</title>

    <!-- Bootstrap -->
    <link href="css/flatly.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/fonts.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>
<body class="<?php if ($user === null): ?>bg-grey<?php endif?>">

    <?php
    // Si l'utilisateur n'est pas null c'est qu'il est connecté, on affiche donc la barre de navigation
        if ($user !== null) {
            include ('navigation.php');
        }
    ?>

    <div class="<?php if ($user !== null): ?>page-wrapper<?php else: ?>container<?php endif?>">
        <div class="rowbox">
            <h3 class="text-uppercase <?php if ($user === null): ?>text-center<?php endif?>">Licences</h3>
        </div>

        <div class="rowbox">
            <div class="charte-container">

                <h3 class="text-uppercase border-bottom">BOOKNET</h3>
                <p>
                    The MIT License (MIT)<br>
                    <br>
                    Copyright (c) 2016 Jean-Baptiste CAPLAN & Seyma TASDEMIR<br>
                    <br>
                    Permission is hereby granted, free of charge, to any person obtaining a copy
                    of this software and associated documentation files (the "Software"), to deal
                    in the Software without restriction, including without limitation the rights
                    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
                    copies of the Software, and to permit persons to whom the Software is
                    furnished to do so, subject to the following conditions:<br>
                    <br>
                    The above copyright notice and this permission notice shall be included in all
                    copies or substantial portions of the Software.<br>
                    <br>
                    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
                    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
                    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
                    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
                    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
                    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
                    SOFTWARE.
                </p>

                <h3 class="text-uppercase border-bottom">CHOSEN</h3>
                by Patrick Filler for Harvest<br>
                Copyright (c) 2011-2016 by Harvest<br>
                <br>
                Available for use under the <a href="https://en.wikipedia.org/wiki/MIT_License">MIT License</a>

                <h3 class="text-uppercase border-bottom">Bootswatch - flatly</h3>
                Copyright (c) 2013 Thomas Park

                Under the <a href="https://en.wikipedia.org/wiki/MIT_License">MIT License</a>

                <h3 class="text-uppercase border-bottom">Font Awesome</h3>
                Font Awesome by Dave Gandy - http://fontawesome.io"<br>
                <br>
                Fonts under <a href="http://scripts.sil.org/OFL">SIL OFL 1.1</a><br>
                Code under <a href="https://en.wikipedia.org/wiki/MIT_License">MIT License</a>
            </div>
        </div>
    </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

<script>
    $('.equaliseh').each(function () {
        var height = 0;
        $($(this).data('target'), $(this)).each(function() {
            if ($(this).height() > height) {
                height = $(this).height()
            }
        });
        $($(this).data('target'), $(this)).height(height)
    })
</script>
</body>
</html>