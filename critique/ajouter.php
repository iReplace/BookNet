<?php

    session_start();
    // On vérifie que l'utilisateur est connecté
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        $user = $_SESSION['user'];
    } else {
        // Sinon on l'envoie vers la page "vitrine" qui présente les fonctions et propose de s'inscrire
        header('Location: ../login.php');
        // TODO: Page vitrine et redirection
        exit('Redirection... <a href="">Cliquez ici</a>');
    }
    // On définit la route actuelle pour l'affichage dans la navigation
    $route = 'critique';

    // On créer un tableau global pour stocker les potentielles erreurs
    $GLOBALS['erreurs'] = array();

    // On crée un dictionnaire global pour enregistrer les informations des différents champs du formulaire
    $GLOBALS['dictionnaire'] = array(
        'critique_livre_id' => array('entity' => 'livre', 'label' => 'livre', 'type' => 'entity')
    );

    // Tentative connexion à la base de données
    try {
        $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    } catch (Exception $e) {
        // En cas d'erreur on quitte proprement en affichant un message controllé
        die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
    }


    // Récupération de la liste des livres couplé avec les informations de l'auteur du livre
    $query = $db->query("SELECT
                          livre.id, livre.titre,
                           -- Si l'auteur n'a pas de pseudo on récupère une concaténée de son prenom et nom
                          IFNULL(auteur.pseudo, CONCAT(auteur.prenom, ' ', auteur.nom)) AS auteur_shortname
                          FROM livre
                          -- jointure avec la table auteur pour récupérer les informations sur l'auteur du livre
                          LEFT JOIN auteur ON livre.auteur_id = auteur.id");
    $livres = $query->fetchAll();

    // valeurs par défaut
    $critique_livre_id = 0;

    // Si une valeur est fournit on la récupère
    if (isset($_GET['livre_id'])) {
        $critique_livre_id = intval($_GET['livre_id']);
    }

    // Si on détecte une valeur dans $_POST, c'est ce que le formulaire à été soumis
    if (isset($_POST['critique_livre_id'])) {
        $critique_livre_id = retrieve_input('critique_livre_id');

        // On vérifie qu'il n'y aucune erreur
        if (count($GLOBALS['erreurs']) == 0) {
            // Valeur par défaut
            $critique_content = array();
            // On procède à l'ajout de la critique
            $query = $db->prepare('INSERT INTO article VALUES(NULL, :utilisateur_id, :livre_id, "", NOW(), NOW(), :content)');
            $query->execute(array(
                ':utilisateur_id' => $user['id'],
                ':livre_id' => $critique_livre_id,
                ':content' => json_encode($critique_content)
            ));

            // On récupère l'id du livre que l'on vient d'ajouter
            $query = $db->query('SELECT id FROM article ORDER BY id DESC LIMIT 0, 1');
            $critique_id = $query->fetch()['id'];

            // On redirige vers l'editeur
            header('Location: editeur.php?critique_id='.$critique_id);
            exit('Redirection... <a href="editeur.php?critique_id='.$critique_id.'">Cliquez ici</a>');
        }

    }

    function retrieve_input($input_name) {
        // On récupère la valeur depuis $_POST
        $value = $_POST[$input_name];
        $informations = $GLOBALS['dictionnaire'][$input_name];
        if ($informations['type'] == 'string') {
            // On convertit value en string
            $value = strval($value);
            // On récupère la longeur minimum ou 1 si elle n'est pas définit
            $minlength = (isset($informations['minlength'])) ? $informations['minlength'] : 1;
            // On récupère la longeur maximum ou 2 si elle n'est pas définit
            $maxlength = (isset($informations['maxlength'])) ? $informations['maxlength'] : 4;
            // Si la longueur de la chaine dépasse la longueur maximum ou est inférieur à la longueur minimum on ajoute un erreur
            if (strlen($value) < $minlength || strlen($value) > $maxlength) {
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "La longueur de $label n'est pas respectée (entre $minlength et $maxlength caractères)";
            }
            // Si aucune valeur n'est spécifiée on ajoute une erreur
            if ($value == '') {
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "Vous devez remplir \"$label\"";
            }

            // On sécurise la chaine de caractère en échappant tous les caractères spécifique à l'HTML
            $value = htmlspecialchars($value);
        } elseif ($informations['type'] == 'integer') {
            // Si aucune valeur n'est précisé on ajoute une erreur
            if ($value == '') {
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "Vous devez remplir \"$label\"";
            }
            // On convertit value en un entier
            $value = intval($value);
            // Si la valeur spécifiée dépasse les borne défini on ajoute une erreur
            if ($value < $informations['min'] || $value > $informations['max']) {
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "La valeur de $label n'est pas respectée";
            }
        } elseif ($informations['type'] == 'entity') {
            $value = intval($value);
            // Tentative connexion à la base de données
            try {
                $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
            } catch (Exception $e) {
                // En cas d'erreur on quitte proprement en affichant un message controllé
                die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
            }
            // On prépare un requête pour vérifier si l'entité existe bel et bien
            $query = $db->prepare('SELECT * FROM '.$informations['entity'].' WHERE id = ?');
            $query->execute(array(
                $value
            ));
            if ($query->rowCount() != 1) {
                // Si elle n'existe pas on ajoute une erreur
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "La valeur de $label n'est pas respectée";
            }
        }

        // On retourne la valeur
        return $value;
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Créer une critique</title>

        <!-- Bootstrap -->
        <link href="../css/flatly.min.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet">
        <link href="../css/fonts.css" rel="stylesheet">
        <link href="../css/chosen.min.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">

    </head>
    <body>
        <?php include('../navigation.php') ?>

        <div class="page-wrapper">

            <div class="rowbox">
                <h2>Nouvelle critique</h2>

                <hr>

                <form method="post" class="container">
                    <div class="form-group">
                        <label>Sélectionner votre livre</label>
                        <select name="critique_livre_id" class="form-control chosen-select">
                            <?php foreach($livres as $livre): ?>
                                <option value="<?php echo $livre['id'] ?>" <?php if($livre['id'] == $critique_livre_id): ?>selected<?php endif ?>><b><?php echo $livre['titre'] ?></b>, <?php echo $livre['auteur_shortname'] ?></option>
                            <?php endforeach ?>
                        </select>
                        <span class="help-block">Vous ne trouvez pas votre livre ? <a href="../livre/ajouter.php">Ajouter le !</a></span>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="utilisateur.php" class="btn btn-primary btn-outline btn-block"><i class="fa fa-list fa-fw"></i> Mes critiques</a>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-success btn-block">Créer !</button>
                        </div>
                    </div>
                </form>
            </div>
            <footer>
                <div class="text-center">
                    <a href="../mentions.php">Mentions légales</a> - <a href="../charte.php">Charte d'utilisation</a> - <a href="../licences.php">Licences</a>
                </div>
            </footer>
        </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../js/bootstrap.min.js"></script>
        <!-- Chosen -->
        <script src="../js/chosen.jquery.min.js"></script>

        <script>
            $('.chosen-select').chosen();
        </script>
    </body>
</html>