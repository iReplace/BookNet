<?php

    session_start();
    // On vérifie que l'utilisateur est connecté
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        $user = $_SESSION['user'];
    } else {
        // Sinon on l'envoie vers la page "vitrine" qui présente les fonctions et propose de s'inscrire
        header('Location: ../login.php');
        // TODO: Page vitrine et redirection
        exit('Redirection... <a href="">Cliquez ici</a>');
    }
    // On définit la route actuelle pour l'affichage dans la navigation
    $route = 'critique';

    // Si aucun critique_id n'est spécifier on renvoie vers la liste des critique
    if (!isset($_GET['critique_id'])) {
        header('Location: utilisateur.php');
        exit('Redirection... <a href="utilisateur.php">Cliquez ici</a>');
    }

    $GLOBALS['erreurs'] = array();

    $GLOBALS['dictionnaire'] = array(
        'critique_titre' => array('maxlength' => 75, 'label' => 'Titre', 'type' => 'string'),
    );

    $critique_id = $_GET['critique_id'];

    // Tentative connexion à la base de données
    try {
        $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    } catch (Exception $e) {
        // En cas d'erreur on quitte proprement en affichant un message controllé
 die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
    }

    // Cette requête récupère les données de la critique ainsi que les données du livre associer plus celle de l'auteur du livre
    $query = $db->prepare('SELECT
                          article.id AS article_id,
                          article.titre,
                          article.date_creation,
                          article.date_edition,
                          article.contenu,
                          article.utilisateur_id,
                          livre.id AS livre_id,
                          livre.titre AS livre_titre,
                          livre.date AS livre_date,
                          auteur.id AS auteur_id,
                          IF(auteur.pseudo IS NULL, CONCAT(auteur.prenom, \' \', auteur.nom), auteur.pseudo) AS auteur_shortname
                          FROM article
                          LEFT JOIN livre ON livre.id = article.livre_id
                          LEFT JOIN auteur ON auteur.id = livre.auteur_id
                          WHERE article.id = :critique_id');
    // On execute la requête en passant en argument l'id de la critique demandée
    $query->execute(array(':critique_id' => $critique_id));

    // Si aucune critique n'est trouvé, on renvoie vers la liste avec un code erreur
    if ($query->rowCount() != 1) {
        header('Location: utilisateur.php?code=404');
        exit('Redirection... <a href="utilisateur.php?code=404">Cliquez ici</a>');
    }

    // On récupère les données de la critique
    $critique = $query->fetch();

    // Si l'utilisateur n'est pas le propriétaire de la critique on le renvoie avec un code de manque de permission
    if ($critique['utilisateur_id'] != $user['id']) {
        header('Location: utilisateur.php?code=403');
        exit('Redirection... <a href="utilisateur.php?code=403">Cliquez ici</a>');
    }

    // On place une valeur par défaut
    $contenuDecode = json_decode($critique['contenu'], true);

    // Si il y'a une valeur dans le formulaire
    if (isset($_POST['critique_contenu'])) {
        $critique_contenu_raw = $_POST['critique_contenu'];
        $critique_titre = retrieve_input('critique_titre');
        $contenuDecode = json_decode($critique_contenu_raw, true);

        // Valeur par défaut
        $critique_contenu = array();
        foreach ($contenuDecode as $el) {
            $element = array();
            // Propriétés générales
            $element['type'] = $el['type'];

            // Cas particulier
            if ($el['type'] == 'quote') {
                $element['source'] = htmlspecialchars($el['source']);
            }

            $element['contenu'] = htmlspecialchars($el['contenu']);

            // On ajoute l'élément
            $critique_contenu[] = $element;
        }

        $critique_contenu = json_encode($critique_contenu);

        // Si il n'y pas d'erreur on continue
        if (count($GLOBALS['erreurs']) == 0) {
            $query = $db->prepare('UPDATE article SET titre = :titre, contenu = :contenu, date_edition = NOW() WHERE id = :critique_id');
            $query->execute(array(
                ':titre' => $critique_titre,
                ':contenu' => $critique_contenu,
                ':critique_id' => $critique_id,
            ));
        }
        header('Location: lire.php?critique_id='.$critique_id);
        exit('Redirection... <a href="lire.php?critique_id='.$critique_id.'">Cliquez ici</a>');

    }

    // Fonction
    function retrieve_input($input_name) {
        // On récupère la valeur depuis $_POST
        $value = $_POST[$input_name];
        $informations = $GLOBALS['dictionnaire'][$input_name];
        if ($informations['type'] == 'string') {
            // On convertit value en string
            $value = strval($value);
            // On récupère la longeur minimum ou 1 si elle n'est pas définit
            $minlength = (isset($informations['minlength'])) ? $informations['minlength'] : 1;
            // On récupère la longeur maximum ou 2 si elle n'est pas définit
            $maxlength = (isset($informations['maxlength'])) ? $informations['maxlength'] : 4;
            // Si la longueur de la chaine dépasse la longueur maximum ou est inférieur à la longueur minimum on ajoute un erreur
            if (strlen($value) < $minlength || strlen($value) > $maxlength) {
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "La longueur de $label n'est pas respectée (entre $minlength et $maxlength caractères)";
            }
            // Si aucune valeur n'est spécifiée on ajoute une erreur
            if ($value == '') {
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "Vous devez remplir \"$label\"";
            }

            // On sécurise la chaine de caractère en échappant tous les caractères spécifique à l'HTML
            $value = htmlspecialchars($value);
        } elseif ($informations['type'] == 'integer') {
            // Si aucune valeur n'est précisé on ajoute une erreur
            if ($value == '') {
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "Vous devez remplir \"$label\"";
            }
            // On convertit value en un entier
            $value = intval($value);
            // Si la valeur spécifiée dépasse les borne défini on ajoute une erreur
            if ($value < $informations['min'] || $value > $informations['max']) {
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "La valeur de $label n'est pas respectée";
            }
        } elseif ($informations['type'] == 'entity') {
            $value = intval($value);
            // Tentative connexion à la base de données
            try {
                $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
            } catch (Exception $e) {
                // En cas d'erreur on quitte proprement en affichant un message controllé
                die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
            }
            // On prépare un requête pour vérifier si l'entité existe bel et bien
            $query = $db->prepare('SELECT * FROM '.$informations['entity'].' WHERE id = ?');
            $query->execute(array(
                $value
            ));
            if ($query->rowCount() != 1) {
                // Si elle n'existe pas on ajoute une erreur
                $label = $informations['label'];
                $GLOBALS['erreurs'][] = "La valeur de $label n'est pas respectée";
            }
        }

        // On retourne la valeur
        return $value;
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head contenu must come *after* these tags -->
        <title>Editer une critique</title>

        <!-- Bootstrap -->
        <link href="../css/flatly.min.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet">
        <link href="../css/fonts.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">

    </head>
    <body>
        <?php include('../navigation.php') ?>

        <div class="page-wrapper">

            <div class="rowbox">
                <div class="row">
                    <div class="col-sm-offset-2 col-sm-1">
                        <p style="padding-top: 20px" class="text-center"><i class="fa fa-book fa-5x"></i></p>
                    </div>
                    <div class="col-sm-9">
                        <h2><em><strong><?php echo $critique['livre_titre'] ?></strong></em></h2>
                        <h4><a href="../auteur/fiche.php?auteur_id=<?php echo $critique['auteur_id'] ?>&from=<?php echo $critique['livre_id'] ?>"><?php echo $critique['auteur_shortname'] ?></a></h4>
                    </div>
                </div>
                <div style="margin-top: 10px" class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <hr>
                    </div>
                </div>

                <?php if (count($GLOBALS['erreurs']) > 0): ?>
                    <div class="alert alert-danger" role="alert">
                        <ul>
                            <? foreach($GLOBALS['erreurs'] as $erreur): ?>
                                <li><?php echo $erreur ?></li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                <?php endif ?>

                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="form-group">
                            <label>Titre</label>
                            <input type="text" form="editeur-form" class="form-control" name="critique_titre" placeholder="Titre de votre critique" maxlength="75" value="<?php echo $critique['titre'] ?>">
                            <div class="help-block">
                               Nous vous rappelons que le contenu de votre critique doit respecter les termes de la <a href="../charte.php">charte d'utilisation</a> que vous avez lu et accepté lors de cotre inscription
                            </div>
                        </div>
                    </div>
                </div>


                <div class="editeur-toolbar">
                    <p class="text-center">
                        <a class="btn btn-success btn-sm" data-type="header">Titre</a>
                        <a class="btn btn-success btn-sm" data-type="paragraph">Paragraphe</a>
                        <a class="btn btn-success btn-sm" data-type="quote">Citation</a>
                    </p>
                </div>

                <hr>

                <div class="editeur"><?php
                        foreach($contenuDecode as $el) {
                            if ($el['type'] == 'paragraph') {
                                ?>
                                <div class="cell-container" data-type="paragraph">
                                    <div class="toolbar">
                                        <i class="fa fa-arrow-up"></i>
                                        <i class="fa fa-arrow-down"></i>
                                        <i class="fa fa-times"></i>
                                    </div>
                                    <p class="editable has-toolbar contenu" contenteditable="true"><?php echo(nl2br($el['contenu'])) ?></p>
                                </div>
                                <?php
                            } elseif ($el['type'] == 'header') {
                                ?>
                                <div class="cell-container" data-type="header">
                                    <div class="toolbar">
                                        <i class="fa fa-arrow-up"></i>
                                        <i class="fa fa-arrow-down"></i>
                                        <i class="fa fa-times"></i>
                                    </div>
                                    <h3 class="editable has-toolbar contenu" contenteditable="true" data-placeholder="Votre titre"><?php echo(nl2br($el['contenu'])) ?></h3>
                                </div>
                                <?php
                            } elseif ($el['type'] == 'quote') {
                                ?>
                                <blockquote class="cell-container" data-type="quote">
                                    <div class="toolbar">
                                        <i class="fa fa-arrow-up"></i>
                                        <i class="fa fa-arrow-down"></i>
                                        <i class="fa fa-times"></i>
                                    </div>
                                    <p class="editable has-toolbar contenu" contenteditable="true" data-placeholder="Votre citation"><?php echo(nl2br($el['contenu'])) ?></p>
                                    <p class="editable text-muted source" contenteditable="true" data-placeholder="Source"><?php echo(nl2br($el['source'])) ?></p>
                                </blockquote>
                                <?php
                            }

                        }
                    ?></div>
                <hr>
                <div class="editeur-footbar">
                    <p class="text-right">
                        <a href="supprimer.php?critique_id=<?php echo $critique['article_id'] ?>" class="btn btn-danger btn-sm btn-outline" onclick="return confirm('Êtes-vous sur de vouloir supprimer votre critique ?')">Supprimer</a>
                        <a class="btn btn-success btn-sm">Sauvegarder</a>
                    </p>
                </div>

            </div>
            <footer>
                <div class="text-center">
                    <a target="_blank" href="../mentions.php">Mentions légales</a> - <a target="_blank" href="../charte.php">Charte d'utilisation</a> - <a target="_blank" href="../licences.php">Licences</a>
                </div>
            </footer>

        </div>

        <div class="hidden editeur-components">
            <div class="quote">
                <blockquote class="cell-container" data-type="quote">
                    <div class="toolbar">
                        <i class="fa fa-arrow-up"></i>
                        <i class="fa fa-arrow-down"></i>
                        <i class="fa fa-times"></i>
                    </div>
                    <p class="editable has-toolbar contenu" contenteditable="true" data-placeholder="Votre citation"></p>
                    <p class="editable text-muted source" contenteditable="true" data-placeholder="Source"><?php echo $critique['livre_titre'] ?> (<?php echo $critique['livre_date'] ?>), <?php echo $critique['auteur_shortname'] ?></p>
                </blockquote>
            </div>
            <div class="paragraph">
                <div class="cell-container" data-type="paragraph">
                    <div class="toolbar">
                        <i class="fa fa-arrow-up"></i>
                        <i class="fa fa-arrow-down"></i>
                        <i class="fa fa-times"></i>
                    </div>
                    <p class="editable has-toolbar contenu" contenteditable="true" data-placeholder="Votre texte"></p>
                </div>
            </div>
            <div class="header">
                <div class="cell-container" data-type="header">
                    <div class="toolbar">
                        <i class="fa fa-arrow-up"></i>
                        <i class="fa fa-arrow-down"></i>
                        <i class="fa fa-times"></i>
                    </div>
                    <h3 class="editable has-toolbar contenu" contenteditable="true" data-placeholder="Votre titre"></h3>
                </div>
            </div>
        </div>

        <form method="post" id="editeur-form" class="hidden editeur-form">
            <input type="hidden" name="critique_contenu" value="">
        </form>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../js/bootstrap.min.js"></script>
        <!-- Editeur -->
        <script src="../js/editeur.js"></script>

        <script>
            $('.equaliseh').each(function () {
                var height = 0;
                $($(this).data('target'), $(this)).each(function() {
                    if ($(this).height() > height) {
                        height = $(this).height()
                    }
                });
                $($(this).data('target'), $(this)).height(height)
            })
        </script>
    </body>
</html>