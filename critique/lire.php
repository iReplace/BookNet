<?php

    session_start();
    // On vérifie que l'utilisateur est connecté
    if (isset($_SESSION['logged_in']) && $_SESSION['logged_in']) {
        $user = $_SESSION['user'];
    } else {
        // Sinon on l'envoie vers la page "vitrine" qui présente les fonctions et propose de s'inscrire
        header('Location: ../login.php');
        // TODO: Page vitrine et redirection
        exit('Redirection... <a href="">Cliquez ici</a>');
    }
    // On définit la route actuelle pour l'affichage dans la navigation
    $route = 'critique';

    // Si aucun critique_id n'est spécifier on renvoie vers la liste des critique
    if (!isset($_GET['critique_id'])) {
        header('Location: index.php');
        exit('Redirection... <a href="index.php">Cliquez ici</a>');
    }

    $critique_id = $_GET['critique_id'];

    // Tentative connexion à la base de données
    try {
        $db = new PDO('mysql:host=localhost;dbname=bibliotheque', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    } catch (Exception $e) {
        // En cas d'erreur on quitte proprement en affichant un message controllé
        die("Une erreur est survenue lors de la connexion à la base de données, veuillez réessayer plus tard");
    }

    // Cette requête récupère les données de la critique ainsi que les données du livre associer plus celle de l'auteur du livre
    $utilisateur_id = $user['id'];
    $query = $db->prepare("SELECT
                            article.id AS critique_id,
                            IF(article.titre = '', 'SANS TITRE', article.titre) AS critique_titre,
                            article.date_creation,
                            article.date_edition,
                            article.contenu,
                            utilisateur.id as utilisateur_id,
                            CONCAT(utilisateur.prenom, ' ', utilisateur.nom) AS utilisateur_fullname,
                            livre.id AS livre_id,
                            livre.titre AS livre_titre,
                            auteur.id AS auteur_id,
                            -- Si l'auteur n'a pas de pseudo on récupère une chaine concaténée de son prenom et nom
                            IFNULL(auteur.pseudo, CONCAT(auteur.prenom, ' ', auteur.nom)) AS auteur_shortname,
                            -- On utilise des sous-requete pour récupérer le nombre de likes et commentaires
                            -- On utilise les ` pour échapper LIKE qui est un mot clé SQL
                            (SELECT COUNT(*) FROM `like` WHERE `like`.article_id = article.id) AS count_like,
                            -- On utilise les ` pour échapper LIKE qui est un mot clé SQL
                            EXISTS (SELECT * FROM `like` WHERE `like`.article_id = article.id AND `like`.utilisateur_id = $utilisateur_id) AS utilisateur_like,
                            (SELECT COUNT(*) FROM commentaire WHERE commentaire.article_id = article.id) AS count_commentaire
                            FROM article
                            LEFT JOIN utilisateur ON utilisateur.id = article.utilisateur_id
                            LEFT JOIN livre ON livre.id = article.livre_id
                            LEFT JOIN auteur ON auteur.id = livre.auteur_id
                            WHERE
                            article.id = :critique_id
                            GROUP BY article.id");
    $query->execute(array(
        ':critique_id' => $critique_id
    ));

    // Si aucune critique n'est trouvé, on renvoie vers la liste avec un code erreur
    if ($query->rowCount() != 1) {
        header('Location: index.php');
        exit('Redirection... <a href="index.php">Cliquez ici</a>');
    }

    // On récupère les données de la critique
    $critique = $query->fetch();

    // On execute une requête pour aller chercher les commentaires associés à la critique
    $query = $db->prepare("SELECT
                            commentaire.id AS commentaire_id,
                            commentaire.contenu,
                            CONCAT(utilisateur.prenom, ' ', utilisateur.nom) AS utilisateur_fullname,
                            DATE_FORMAT(commentaire.date, 'le %e/%m/%y à %H:%i') AS date_format
                            FROM commentaire_b
                            -- On fait une jointure avec utilisateurs pour récupérer les informations de l'auteur du commentaire
                            LEFT JOIN utilisateur ON utilisateur.id = commentaire.utilisateur_id
                            WHERE
                            commentaire.article_id = ?
                            -- On ordonne de manière chronologique inversé
                            ORDER BY commentaire.date DESC");
    $query->execute(array($critique['critique_id']));
    $critique['commentaires'] = $query->fetchAll();

    # On vérfie si on a transmis un livre via "from"
    if (isset($_GET['from'])) {
        $livre_id = $_GET['from'];

        // préparation de la requête pour récupérer le livre
        $query = $db->prepare('SELECT
                               *
                               FROM livre
                               WHERE
                               id = ?');
        $query->execute(array($livre_id));
        if ($query->rowCount() == 1) {
            // Si il y'a bien un livre on stocke ses infos dans une variable
            $livre_from = $query->fetch();
        } else {
            // Sinon on place la valeur de la variable sur null, c'est à dire rien
            $livre_from = null;
        }
    }

    // On place une valeur par défaut
    $contenuDecode = json_decode($critique['contenu'], true);

?>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head contenu must come *after* these tags -->
        <title>Lire <?php echo $critique['critique_titre'] ?></title>

        <!-- Bootstrap -->
        <link href="../css/flatly.min.css" rel="stylesheet">
        <link href="../css/font-awesome.min.css" rel="stylesheet">
        <link href="../css/fonts.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">

    </head>
    <body>
        <?php include('../navigation.php') ?>

        <div class="page-wrapper">

            <div class="rowbox">
                <div class="row">
                    <div class="col-md-1 col-md-offset-2 col-sm-offset-1 col-sm-2">
                        <p style="padding-top: 30px" class="text-center"><i class="fa fa-bookmark fa-5x"></i></p>
                    </div>
                    <div class="col-sm-9">
                        <h2 class="critique-titre">
                            <strong><?php echo $critique['critique_titre'] ?></strong> <small class="text-danger"><i class="fa fa-heart fa-fw"></i> <?php echo $critique['count_like'] ?></small> <small class="text-success"><i class="fa fa-comment fa-fw"></i> <?php echo $critique['count_commentaire'] ?></small> <br>
                            <small class="small-indent"><?php echo $critique['utilisateur_fullname'] ?></small>
                        </h2>
                        <h2 class="critique-livre">
                            <small><i><a target="_self" href="../livre/fiche.php?livre_id=<?php echo $critique['livre_id'] ?>" target="_blank"><i class="fa fa-book fa-fw"></i> <?php echo $critique['livre_titre'] ?></a></i></small>
                        </h2>
                    </div>
                </div>
                <div style="margin-top: 15px" class="row">
                    <div class="col-sm-7 col-sm-offset-2">
                        <div class="col-xs-6">
                            <a href="like.php?critique_id=<?php echo $critique['critique_id'] ?>" class="btn btn-success btn-outline btn-block btn-sm">
                                <?php if ($critique['utilisateur_like'] > 0): ?>
                                    <i class="fa fa-heart fa-fw"></i> Je n'aime plus
                                <?php else: ?>
                                    <i class="fa fa-heart-o fa-fw"></i> J'aime
                                <?php endif ?>
                            </a>
                        </div>
                        <div class="col-xs-6">
                            <?php if ($user['id'] == $critique['utilisateur_id']): ?>
                                <a href="editeur.php?critique_id=<?php echo $critique['critique_id'] ?>" class="btn btn-success btn-outline btn-block btn-sm"><i class="fa fa-paragraph fa-fw"></i> Modifier</a>
                            <?php else: ?>
                                <a href="../profil/index.php?utilisateur_id=<?php echo $critique['utilisateur_id'] ?>&from=<?php echo $critique['critique_id'] ?>" class="btn btn-success btn-outline btn-block btn-sm"><i class="fa fa-user fa-fw"></i> Profil de l'utilisateur</a>
                            <?php endif ?>
                        </div>
                        <div class="col-sm-12">
                            <hr style="margin-top: 7px; margin-bottom: 7px;">
                        </div>
                        <div class="col-sm-12">
                            <?php if (isset($livre_from) && $livre_from): ?>
                                <a class="btn btn-primary btn-outline btn-block btn-sm" href="../livre/fiche.php?livre_id=<?php echo $livre_from['id'] ?>"><i class="fa fa-book fa-fw"></i> <i><?php echo $livre_from['titre'] ?></i></a>
                            <?php else: ?>
                                <a class="btn btn-primary btn-outline btn-block btn-sm" href="index.php"><i class="fa fa-list fa-fw"></i> Liste des critiques</a>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="rowbox">
                <div class="container-fluid"><?php
                        foreach($contenuDecode as $el) {
                            if ($el['type'] == 'paragraph') {
                                ?>
                                <div class="cell-container">
                                    <p class="contenu"><?php echo(nl2br($el['contenu'])) ?></p>
                                </div>
                                <?php
                            } elseif ($el['type'] == 'header') {
                                ?>
                                <div class="cell-container">
                                    <h3 class="contenu"><?php echo(nl2br($el['contenu'])) ?></h3>
                                </div>
                                <?php
                            } elseif ($el['type'] == 'quote') {
                                ?>
                                <blockquote class="cell-container">
                                    <p class="has-toolbar contenu"><?php echo(nl2br($el['contenu'])) ?></p>
                                    <p class="text-muted source"><?php echo(nl2br($el['source'])) ?></p>
                                </blockquote>
                                <?php
                            }
                        }
                    ?></div>
                <hr>
            </div>

            <div class="rowbox">

                <div class="commentaires-container">

                    <h4 class="text-uppercase border-bottom">Commentaires</h4>
                    <div class="well well-sm">
                        <p style="padding: 10px">Vous avez aimé ? Vous êtes en accord avec l'auteur ou y'a-t-il quelques points sur lesquels votre avis est différent ? <br>N'hésitez pas à poster un commentaire !</p>
                        <div class="editable bg-white form-control" contenteditable="true" data-placeholder="Donner votre avis !"></div>
                        <small class="help-block text-right">Nous vous rappelons que pour garder les échanges courtois votre commentaire ne doit pas enfreindre les règles de notre <a href="../charte.php">charte d'utilisation</a></small>
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <a class="btn btn-success btn-sm btn-block">Poster</a>
                            </div>
                        </div>
                    </div>

                    <?php if (count($critique['commentaires']) == 0): ?>
                        <p class="text-center">
                            Aucun commentaire pour l'instant
                        </p>
                    <?php endif ?>

                    <?php foreach($critique['commentaires'] as $commentaire): ?>
                        <div class="commentaire" id="C<?php echo $commentaire['commentaire_id'] ?>">
                            <div class="head">
                                <img class="img-circle" src="../image/avatar/default_profile.png">
                                <p class="text-uppercase utilisateur-fullname"><?php echo $commentaire['utilisateur_fullname'] ?></p>
                            </div>
                            <div class="body">
                                <div class="date">
                                    <?php echo $commentaire['date_format'] ?>
                                </div>
                                <div class="contenu">
                                    <p><?php echo nl2br($commentaire['contenu']) ?></p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>

            </div>
            <footer>
                <div class="text-center">
                    <a href="../mentions.php">Mentions légales</a> - <a href="../charte.php">Charte d'utilisation</a> - <a href="../licences.php">Licences</a>
                </div>
            </footer>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="../js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../js/bootstrap.min.js"></script>

        <script>

            // Credit: Chris West
            String.prototype.replaceAll = function(target, replacement) {
                return this.split(target).join(replacement);
            };

            $('.btn-success').click(function(){
                var commentaire = clear($('.editable').html());
                if (commentaire != '') {
                    $.ajax({
                        url: 'post.php',
                        method: 'POST',
                        data: {critique_id: "<?php echo $critique['critique_id'] ?>", commentaire_contenu: commentaire},
                        success: function(data, textStatus, jqXHR) {
                            window.location.reload();
                        },
                        error: function(xhr, status, error) {
                            var text = 'Une erreur survenu lors de l\'ajout de votre commentaire';
                            console.log(xhr);
                            if (xhr.responseText != '') {
                                text = xhr.responseText;
                            }
                            alert(text);
                        },
                        dataType: 'json'
                    });
                }
            });

            function clear(string) {
                string = String(string);
                string = string.replaceAll('<br>', '');
                string = string.replaceAll('</div>', '');
                string = string.replace('<div>', '\n');
                string = string.replace('&nbsp;', ' ');

                return string;
            }
        </script>
    </body>
</html>